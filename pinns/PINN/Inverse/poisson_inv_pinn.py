import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils
import torch.utils.data
from torch.utils.data import DataLoader
import matplotlib.pyplot as plt
import numpy as np
from math import pi
from PINN import NeuralNet
from PINN import init_xavier
import time
import argparse
from sklearn.metrics import mean_squared_error
import csv
import os
import pylab as p
import matplotlib.cm as cm
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.colors import Normalize
import matplotlib.ticker as ticker

device = torch.device('cpu')

# Define the exact solution
def exact_solution(x, y):
    return torch.sin(pi * x**2) * torch.sin( pi * y**2)

random_state = 1234
n_measurement_points = 50

rng = torch.Generator().manual_seed(random_state)
measurement_points = torch.rand(n_measurement_points, 2) * 1.4
u_measured = exact_solution(measurement_points[:,0], measurement_points[:,1]).reshape((-1, 1))

# assigning number of points
bottom_boundary_pts = 375
left_boundary_pts = 375
right_boundary_pts = 375
top_boundary_pts = 375
residual_pts = 1500

# Type of optimizer (ADAM or LBFGS)
opt_type = "LBFGS"

xb_bottom = 1.4*torch.rand((bottom_boundary_pts,1))
yb_bottom =  torch.zeros((bottom_boundary_pts, 1))
b_bottom =  torch.cat([xb_bottom, yb_bottom],1).to(device)
u_b_b = exact_solution(b_bottom[:,0], b_bottom[:,1]).reshape(-1, 1)

xb_left = torch.zeros((left_boundary_pts, 1))
yb_left = 1.4*torch.rand((left_boundary_pts, 1))
b_left = torch.cat([xb_left, yb_left ],1).to(device)
u_b_l = exact_solution(b_left[:,0], b_left[:,1]).reshape(-1, 1)

xb_right = 1.4*torch.ones((right_boundary_pts, 1)) # right spatial boundary
yb_right = 1.4*torch.rand((right_boundary_pts, 1)) # right boundary pts
b_right = torch.cat([xb_right, yb_right ],1).to(device)
u_b_r = exact_solution(b_right[:,0], b_right[:,1]).reshape(-1, 1)

xb_top = 1.4*torch.rand((top_boundary_pts, 1))
yb_top = 1.4*torch.ones((top_boundary_pts, 1))
b_top =  torch.cat([xb_top, yb_top],1).to(device)
u_b_t = exact_solution(b_top[:,0], b_top[:,1]).reshape(-1, 1)

x_interior = 1.4*torch.rand((residual_pts, 1))
y_interior = 1.4*torch.rand((residual_pts, 1))
interior = torch.cat([x_interior, y_interior],1).to(device)
f = 2 * pi * (torch.cos(y_interior**2 * pi) * torch.sin(pi * x_interior**2) + (torch.cos(pi * x_interior**2)
                - 2*pi*(x_interior**2+y_interior**2)*torch.sin(pi*x_interior**2)) * torch.sin(pi*y_interior**2))


training_set = DataLoader(torch.utils.data.TensorDataset(b_bottom.to(device), u_b_b.to(device),
                                                         b_left.to(device),  u_b_l.to(device),
                                                         b_right.to(device), u_b_r.to(device),
                                                         b_top.to(device), u_b_t.to(device)), batch_size=375, shuffle=False)

# Model definition
my_network = NeuralNet(input_dimension = b_bottom.shape[1], output_dimension = u_b_b.shape[1]+1, n_hidden_layers=4, neurons=20)
my_network = my_network.to(device)

# Random Seed for weight initialization
retrain = 128
# Xavier weight initialization
init_xavier(my_network, retrain)


if opt_type == "ADAM":
    optimizer_ = optim.Adam(my_network.parameters(), lr=0.001)
elif opt_type == "LBFGS":
    optimizer_ = optim.LBFGS(my_network.parameters(), lr=0.1, max_iter=1, max_eval=50000, tolerance_change=1.0 * np.finfo(float).eps)
else:
    raise ValueError("Optimizer not recognized")

def fit(model, training_set, interior, num_epochs, optimizer, p, verbose=True):
    history = list()

    # Loop over epochs
    for epoch in range(num_epochs):
        if verbose: print("################################ ", epoch, " ################################")

        running_loss = list([0])

        # Loop over batches
        for j, (bd_bottom, ubb, bd_left, ubl, bd_right, ubr, bd_top, ubt) in enumerate(training_set):
            def closure():
                # zero the parameter gradients
                optimizer.zero_grad()
                # for bottom boundary
                u_bd_bottom_pred_ = model(bd_bottom)
                # for left boundary
                u_bd_left_pred_ = model(bd_left)
                # for right boundary
                u_bd_right_pred_ = model(bd_right)
                # for top boundary
                u_bd_top_pred_ = model(bd_top)

                # measured predition
                um_pred_ = model(measurement_points)

                # residual calculation
                interior.requires_grad = True
                u_hat_two = model(interior)

                u_hat = u_hat_two[:,0].reshape(-1, 1)
                alpha = u_hat_two[:,1].reshape(-1, 1)
                alpha = torch.mean(alpha)

                inputs = torch.ones(residual_pts, 1).to(device)
                inputs2 = torch.ones(residual_pts, 1).to(device)
                grad_u_hat = torch.autograd.grad(u_hat, interior, grad_outputs=inputs, create_graph=True)[0]

                u_x = grad_u_hat[:, 0].reshape(-1,)
                u_y = grad_u_hat[:, 1].reshape(-1,)

                grad_grad_u_x = \
                torch.autograd.grad(u_x, interior, grad_outputs=torch.ones(1500), create_graph=True)[0]
                u_xx = grad_grad_u_x[:, 0].reshape(-1,)

                grad_grad_u_y = \
                torch.autograd.grad(u_y, interior, grad_outputs=torch.ones(1500), create_graph=True)[0]
                u_yy = grad_grad_u_y[:, 1].reshape(-1,)



                # Item 1. below
                loss = (torch.mean((u_bd_bottom_pred_[:,0].reshape(-1, ) - ubb.reshape(-1, )) ** p) +
                        torch.mean((u_bd_left_pred_[:,0].reshape(-1, ) - ubl.reshape(-1, )) ** p) +
                        torch.mean((u_bd_right_pred_[:,0].reshape(-1, ) - ubr.reshape(-1, )) ** p) +
                        torch.mean((u_bd_top_pred_[:,0].reshape(-1, ) - ubt.reshape(-1, )) ** p) +
                        torch.mean((um_pred_[:, 0].reshape(-1, ) - u_measured.reshape(-1, )) ** p) +
                        0.05*torch.mean((u_xx.reshape(-1, ) + alpha*u_yy.reshape(-1, ) - f.reshape(-1, ) ) ** p))

                # Item 2. below
                loss.backward()
                # Compute average training loss over batches for the current epoch
                running_loss[0] += loss.item()
                return loss

            # Item 3. below
            optimizer.step(closure=closure)

        print('Loss: ', (running_loss[0] / len(training_set)))
        history.append(running_loss[0])

    return history

#n_epochs = 10000
#start_time = time.time()
#history = fit(my_network, training_set, interior, n_epochs, optimizer_, p=2, verbose=True )
#end_time = time.time()
#total_time = end_time - start_time
#print("Training time: {:.2f} seconds".format(total_time))

my_network.load_state_dict(torch.load('Trained_Models/model2.pth'))
my_network = my_network.cpu()

# Create linspace from 0 to 1.4 for 100 points
x_test = torch.linspace(0, 1.4, 256)
t_test = torch.linspace(0, 1.4, 100)

# Create meshgrid
X, T = torch.meshgrid(x_test, t_test)

# Flatten the meshgrid arrays and concatenate them
test = torch.cat([X.reshape(-1, 1), T.reshape(-1, 1)], dim=1)

# Calculate the exact solution and the network's prediction
u_exact = exact_solution(test[:, 0], test[:, 1]).reshape(-1, 1)
u_test_pred = my_network(test)
alpha = u_test_pred[:,1].detach().numpy().mean()
u_test_pred = u_test_pred[:,0].reshape(-1, 1)

print("The learned value of alpha is :", alpha)

u_exact = u_exact.reshape(256, 100)
u_test_pred = u_test_pred.reshape(256, 100)

#torch.save(my_network.state_dict(), 'Trained_Models/model3.pth')

# Compute the relative L2 error norm (generalization error)

RMSE = np.sqrt(mean_squared_error(u_test_pred.detach().numpy(), u_exact.detach().numpy()))
print("RMSE Test: ", RMSE)

u_test_pred = u_test_pred.reshape(-1, )
u_exact = u_exact.reshape(-1, )

abs_err = torch.abs(u_test_pred - u_exact)

relative_error_test = torch.sqrt(torch.mean((u_test_pred - u_exact)**2) / torch.mean(u_exact**2))
print("Relative Error Test: ", relative_error_test.detach().numpy())

# Function to append result to CSV file
def append_to_csv(file_name, result):
    file_exists = os.path.isfile(file_name)
    
    with open(file_name, mode='a', newline='') as file:
        writer = csv.writer(file)
        
        # Write header only if file does not exist
        if not file_exists:
            writer.writerow(['Result'])
        
        # Append the result
        writer.writerow([result])
        
# File names for storing results
RMSE_file = 'Results/RMSE_results.csv'
Rel_err_file = 'Results/Rel_err_results.csv'
Time_file = 'Results/Time_results.csv'
Alpha_file = 'Results/Alpha_results.csv'

# Append results to respective CSV files
#append_to_csv(RMSE_file, RMSE)
#append_to_csv(Rel_err_file, relative_error_test.detach().numpy())
#append_to_csv(Time_file, total_time)
#append_to_csv(Alpha_file, alpha)

import matplotlib.pyplot as plt

# Assuming u_exact, u_test_pred, and abs_err are already defined and measurement_points are provided
u_exact = u_exact.reshape(256, 100)
u_test_pred = u_test_pred.reshape(256, 100)
abs_err = abs_err.reshape(256, 100)

extent = [0, 1.4, 0, 1.4]
# Font size for all text elements
font_size = 16

# Create a figure and a set of subplots
fig, axes = plt.subplots(1, 3, figsize=(9, 3))

# Plot the predicted solution
im1 = axes[0].imshow(u_test_pred.detach().numpy(), extent=extent, origin='lower', aspect='auto', cmap='jet')
axes[0].scatter(measurement_points[:, 0], measurement_points[:, 1], c='black', s=5)
axes[0].set_title('Approximation', fontsize=font_size)
axes[0].set_xlabel('$x_1$', fontsize=font_size)
axes[0].set_ylabel('$x_2$', fontsize=font_size)
cbar1 = plt.colorbar(im1, ax=axes[0], orientation='horizontal', pad=0.3)
cbar1.ax.tick_params(labelsize=font_size)

# Plot the exact solution
im2 = axes[1].imshow(u_exact.detach().numpy(), extent=extent, origin='lower', aspect='auto', cmap='jet')
axes[1].scatter(measurement_points[:, 0], measurement_points[:, 1], c='black', s=5)
axes[1].set_title('Ground truth + data locations', fontsize=font_size)
axes[1].set_xlabel('$x_1$', fontsize=font_size)
axes[1].set_ylabel('$x_2$', fontsize=font_size)
cbar2 = plt.colorbar(im2, ax=axes[1], orientation='horizontal', pad=0.3)
cbar2.ax.tick_params(labelsize=font_size)

# Plot the absolute error
im3 = axes[2].imshow(abs_err.detach().numpy(), extent=extent, origin='lower', aspect='auto', cmap='jet')
axes[2].scatter(measurement_points[:, 0], measurement_points[:, 1], c='black', s=5)
axes[2].set_title('Absolute error', fontsize=font_size)
axes[2].set_xlabel('$x_1$', fontsize=font_size)
axes[2].set_ylabel('$x_2$', fontsize=font_size)
cbar3 = plt.colorbar(im3, ax=axes[2], orientation='horizontal', pad=0.3)
cbar3.ax.tick_params(labelsize=font_size)

# Set the tick parameters for all axes
for ax in axes:
    ax.tick_params(axis='both', labelsize=font_size)

# Adjust layout
plt.tight_layout()
plt.savefig('Results/Figures/PINN_inverse_poisson_soln.pdf', dpi=300, bbox_inches="tight")
plt.show()

