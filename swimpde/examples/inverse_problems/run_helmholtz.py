##
## Run this file to generate the results for the inverse Helmholtz experiment with unknown parameter field.
##

from time import time

import numpy as np
import matplotlib.pyplot as plt

from sklearn.metrics import mean_squared_error, max_error
from sklearn.pipeline import Pipeline

import pandas as pd
from scipy.optimize import least_squares

from swimnetworks import Dense, Linear

import sys

sys.path.append("../../")
sys.path.append("../../src/")
from swimpde import Domain
from swimpde import BasicAnsatz
from swimpde.utils import *

###
## Important parameters (all fixed in this experiment)

# Data
n_dim = 2
n_test = 101  # Number of uniform test points per dimension
Q = 30  # Uniform collocation points
n_measurement_points = 300
random_state = 25
x_lim = [0, 1.5]

# All hyper-parameters
width_hidden_layer = 400  # width of the hidden layer
als_iters = 1  # Iterations for ALS
nn_u_width = width_hidden_layer  # Width of SWIM network approximating u(x)
nn_c_width = width_hidden_layer  # Width of SWIM network approximating c(x)


###
## Problem setup


# Define True solution
def gamma(v):
    return 100.0 * (
        1.0 + 0.25 * (np.sin(2 * np.pi * v[:, 0]) + np.sin(2 * np.pi * v[:, 1]))
    )


def u_true(v):
    term_1 = 5.0 / 2.0 * np.sin(np.pi * v[:, 0] - 2 * np.pi / 5.0) + 3.0 / 2.0 * np.cos(
        2 * np.pi * v[:, 0] + 3.0 * np.pi / 10.0
    )
    term_2 = 5.0 / 2.0 * np.sin(np.pi * v[:, 1] - 2 * np.pi / 5.0) + 3.0 / 2.0 * np.cos(
        2 * np.pi * v[:, 1] + 3.0 * np.pi / 10.0
    )
    return term_1 * term_2


# Define the forcing term
def forcing(v):
    t_12 = 5.0 / 2.0 * np.sin(np.pi * v[:, 0] - 2 * np.pi / 5.0) + 3.0 / 2.0 * np.cos(
        2 * np.pi * v[:, 0] + 3.0 * np.pi / 10.0
    )
    t_34 = 5.0 / 2.0 * np.sin(np.pi * v[:, 1] - 2 * np.pi / 5.0) + 3.0 / 2.0 * np.cos(
        2 * np.pi * v[:, 1] + 3.0 * np.pi / 10.0
    )
    t_1_xx = (-((np.pi) ** 2)) * (5.0 / 2.0 * np.sin(np.pi * v[:, 0] - 2 * np.pi / 5.0))
    t_2_xx = (-4 * (np.pi) ** 2) * (
        3.0 / 2.0 * np.cos(2 * np.pi * v[:, 0] + 3.0 * np.pi / 10.0)
    )
    t_3_yy = (-((np.pi) ** 2)) * (5.0 / 2.0 * np.sin(np.pi * v[:, 1] - 2 * np.pi / 5.0))
    t_4_yy = (-4 * (np.pi) ** 2) * (
        3.0 / 2.0 * np.cos(2 * np.pi * v[:, 1] + 3.0 * np.pi / 10.0)
    )
    laplacian_u = (t_1_xx + t_2_xx) * t_34 + (t_3_yy + t_4_yy) * t_12
    return laplacian_u - gamma(v) * u_true(v)


# Error definitions
def l_inf(v, v_true):
    v = v.ravel()
    v_true = v_true.ravel()
    return (
        np.max(np.abs(v - v_true))
        / np.sqrt(mean_squared_error(v_true, np.zeros_like(v_true))).ravel()
    )


def l_2(v, v_true):
    v = v.ravel()
    v_true = v_true.ravel()
    return np.sqrt(
        mean_squared_error(v_true, v)
        / mean_squared_error(v_true, np.zeros_like(v_true))
    ).ravel()


def rmse(v, v_true):
    v = v.ravel()
    v_true = v_true.ravel()
    return np.sqrt(mean_squared_error(v_true, v)).ravel()


####
# Measurement data, test data, sampled points in space
def setup_data(_random_state):
    rng = np.random.default_rng(_random_state)

    # Collocation points on which the PDE is solved later
    x_col = np.linspace(*x_lim, Q)
    y_col = np.linspace(*x_lim, Q)
    xx_col, yy_col = np.meshgrid(x_col, y_col)
    data_col = np.hstack(
        (xx_col.reshape(-1, 1), yy_col.reshape(-1, 1))
    )  # Uniform evaluation points

    # Test data on which we compute the error later
    x_test = np.linspace(*x_lim, n_test)
    y_test = np.linspace(*x_lim, n_test)
    xx_test, yy_test = np.meshgrid(x_test, y_test)
    data_test = np.hstack(
        (xx_test.reshape(-1, 1), yy_test.reshape(-1, 1))
    )  # Uniform evaluation/test points

    # measurement points where we assume that we have available u data (corrupted by noise)
    data_m = rng.uniform(
        low=x_lim[0], high=x_lim[1], size=(n_measurement_points, n_dim)
    )

    return data_m, data_col, data_test


# Measured data with noise
def generate_noisy_measurements(data_m, _random_state, noise_level):
    u_true_m = u_true(data_m)  # True solution at locations of measurement data
    rng = np.random.default_rng(seed=_random_state)
    u_m = u_true_m * (
        1 + noise_level * rng.uniform(-1.0, 1.0, size=(np.shape(u_true_m)))
    )
    return u_m


def plot_true_solution_and_points(data_m, data_test):
    u_true_test = u_true(data_test)

    # Plots of true solution and coeeficient field
    fig, ax = plt.subplots(1, 2, figsize=(5, 3))
    plt.rcParams["image.cmap"] = "jet"  #'brg'
    extent = [-0, 1.5, -0, 1.5]
    aspect = 1.3
    sol_img1 = ax[0].imshow(
        u_true_test.reshape(n_test, n_test),
        zorder=1,
        extent=extent,
        origin="lower",
        aspect=aspect,
    )  # , vmin=vlim[0], vmax=vlim[1]
    sol_img2 = ax[1].imshow(
        gamma(data_test).reshape(n_test, n_test),
        extent=extent,
        origin="lower",
        aspect=aspect,
    )  # , vmin=vlim[0], vmax=vlim[1]
    sol_img3 = ax[0].scatter(
        *data_m.T, s=1, zorder=2, marker="+"
    )  # , vmin=vlim[0], vmax=vlim[1]

    ax[0].set_aspect("equal")
    ax[1].set_aspect("equal")

    fig.suptitle("Ground truth on test data locations")
    fig.colorbar(sol_img1, ax=ax[0], location="bottom")
    fig.colorbar(sol_img2, ax=ax[1], location="bottom")
    ax[0].set_title("u(x,y)")
    ax[1].set_title("gamma(x,y)")
    fig.tight_layout()


def setup_sampled_network(
    data_m,
    u_true_m,
    regularization_scale,
    activation_function="tanh",
    parameter_sampler="tanh",
    _random_state=1234,
):
    # SWIM network to acompute u(x): Setup
    ansatz = BasicAnsatz(
        activation=activation_function,
        parameter_sampler=parameter_sampler,
        n_neurons=nn_u_width,
        random_state=_random_state,
        regularization_scale=regularization_scale,
    )

    domain_m = Domain(interior_points=data_m, n_dim=2)

    # SWIM network with the initial data: Compute weights, biases and an initial guess for last layer weights
    ansatz.init_model(domain_m, boundary_condition=None, initial_condition=u_true_m)
    last_layer = Linear(regularization_scale=regularization_initialcondition)

    ansatz.fit_model(data_m, u_true_m)
    last_layer.fit(
        ansatz.evaluate_model(data_m), u_true_m
    )  # dont use this result it will fit a bias term which we do not want
    last_layer.weights = np.linalg.lstsq(
        ansatz.evaluate_model(data_m), u_true_m, rcond=regularization_initialcondition
    )[0]
    last_layer.biases = 0 * last_layer.biases

    return Pipeline([("ansatz", ansatz), ("last_layer", last_layer)])


def setup_gamma_network(
    data_col,
    regularization_scale,
    activation_function="tanh",
    parameter_sampler="tanh",
    _random_state=123454,
):
    # SWIM network to acompute c(x): Setup (Can also use the same network with different last layer coefficients)
    model_gamma = Dense(
        layer_width=nn_c_width,
        activation=activation_function,
        parameter_sampler=parameter_sampler,
        random_seed=_random_state,
        sample_uniformly=True,
    )
    model_gamma.fit(
        data_col, 0 * forcing(data_col)
    )  # Sampled uniformly, so output function does not matter here!!

    # just setup the layer, not a real fit.
    last_layer_gamma = Linear(regularization_scale=regularization_initialcondition).fit(
        model_gamma.transform(data_col[:4, ...]), data_col[:4, :1]
    )
    return Pipeline([("ansatz", model_gamma), ("last_layer", last_layer_gamma)])


def alternating_least_squares(
    data_m, u_true_m, data_col, model_u, model_gamma, regularization_scale
):
    u_swim_m_inner = model_u[0].evaluate_model(data_m)
    phi_u = model_u[0].evaluate_model(data_col)
    phi_u_xx = model_u[0].evaluate_model_laplace(data_col)
    forcing_col = forcing(data_col)
    gamma_approx_inner = model_gamma[0].transform(data_col)
    outer_weights_current = model_u[1].weights

    for i in range(als_iters):
        # Least Squares for computing gamma
        matrix_in_gamma = (
            gamma_approx_inner.T
            * (phi_u @ outer_weights_current + model_u[1].biases).ravel()
        ).T
        matrix_out_gamma = (phi_u_xx @ outer_weights_current).ravel() - forcing_col
        gamma_outer_weights_current = np.linalg.lstsq(
            matrix_in_gamma, matrix_out_gamma, rcond=regularization_scale
        )[0]

        # Least squares for computing u_approx: use updated value of gamma
        # and also stack the true values we know at the measurement points
        matrix_in_u = (
            phi_u_xx
            - (gamma_outer_weights_current.T @ gamma_approx_inner.T).reshape((-1, 1))
            * phi_u
        )
        matrix_in_u = np.row_stack([matrix_in_u, u_swim_m_inner])

        matrix_out_u = forcing_col
        matrix_out_u = np.concatenate([matrix_out_u, u_true_m])

        outer_weights_current = np.linalg.lstsq(
            matrix_in_u, matrix_out_u, rcond=regularization_scale
        )[0]

    model_u[1].weights = outer_weights_current.reshape((-1, 1))

    # else:  # use nonlinear least squares
    u_approx_col = (phi_u @ outer_weights_current).ravel()
    u_xx_approx_col = (phi_u_xx @ outer_weights_current).ravel()

    # Non-linear Least Squares
    # Implementation of Non-linear least squares
    def system_combined(v_c):
        outer_weights_current_ = v_c[:nn_u_width]
        gamma_outer_weights_current_ = v_c[nn_u_width:]
        u_approx_col = phi_u @ outer_weights_current_
        u_xx_approx_col = phi_u_xx @ outer_weights_current_
        return np.mean(
            (
                u_xx_approx_col
                - (gamma_outer_weights_current_ @ gamma_approx_inner.T) * u_approx_col
                - forcing_col
            ).ravel()
            ** 2
        ) + np.mean((u_swim_m_inner @ outer_weights_current_ - u_true_m).ravel() ** 2)

    def system_only_gamma(gamma_outer_weights_current_):
        return (
            u_xx_approx_col
            - (gamma_outer_weights_current_ @ gamma_approx_inner.T) * u_approx_col
            - forcing_col
        ).ravel()

    # gamma_outer_weights_current = np.ones((nn_u_width, 1))

    # rng = np.random.default_rng(seed=12)
    # gamma_outer_weights_current = rng.normal(size=(nn_u_width, 1))

    if False:
        find_all_outer_weights = True
        tol = 1e-15
        if find_all_outer_weights:
            res = least_squares(
                system_combined,
                jac="cs",
                x0=np.vstack(
                    (outer_weights_current, gamma_outer_weights_current)
                ).reshape(-1),
                verbose=1,
                ftol=tol,
                gtol=tol,
                xtol=tol,
            )
            model_u[1].weights = res.x[:nn_c_width].ravel()
            gamma_outer_weights_current = res.x[nn_u_width:].ravel()
        else:
            res = least_squares(
                system_only_gamma, gamma_outer_weights_current.ravel(), verbose=1
            )
            gamma_outer_weights_current = res.x.reshape(-1)

    # Compute approximated function gamma(x)
    model_u[1].weights = model_u[1].weights.reshape((-1, 1))
    model_u[1].biases = model_u[1].biases * 0

    model_gamma[1].weights = gamma_outer_weights_current.reshape((-1, 1))
    model_gamma[1].biases = model_gamma[1].biases * 0

    return model_u, model_gamma


def plot_results(data_m, data_col, data_test, model_gamma, model_u):
    gamma_true_col = gamma(data_col)
    u_true_test = u_true(data_test)

    gamma_approx_test = model_gamma.transform(data_test)
    u_approx_test = (
        model_u[1].transform(model_u[0].evaluate_model(data_test))
    ).reshape(-1)
    print(
        "l-inf error on test data = ",
        l_inf(gamma_approx_test.ravel(), gamma(data_test).ravel()),
    )
    print(
        "l-two error on test data = ",
        l_2(gamma_approx_test.ravel(), gamma(data_test).ravel()),
    )

    # Final solution of u(x ,y) on test data
    vlim = [np.min(gamma(data_test)), np.max(gamma(data_test))]
    fig, ax = plt.subplots(1, 3, figsize=(7, 3))
    plt.rcParams["image.cmap"] = "jet"  #'brg'
    extent = x_lim * 2
    aspect = 1.3
    sol_img1 = ax[0].imshow(
        gamma_approx_test.reshape(n_test, n_test),
        zorder=1,
        extent=extent,
        origin="lower",
        aspect=aspect,
        # vmin=vlim[0],
        # vmax=vlim[1],
    )
    sol_img2 = ax[1].imshow(
        gamma(data_test).reshape(n_test, n_test),
        extent=extent,
        origin="lower",
        aspect=aspect,
        # vmin=vlim[0],
        # vmax=vlim[1],
    )
    sol_img3 = ax[2].imshow(
        np.abs(
            gamma_approx_test.reshape(n_test, n_test)
            - gamma(data_test).reshape(n_test, n_test)
        )
        / np.max(np.abs(gamma(data_test))),
        extent=extent,
        origin="lower",
        aspect=aspect,
    )
    fig.suptitle(r"Coefficient field $\gamma(x)$ on test data")

    fig.colorbar(sol_img1, ax=ax[0], location="bottom")
    fig.colorbar(sol_img2, ax=ax[1], location="bottom")
    fig.colorbar(sol_img3, ax=ax[2], location="bottom")
    ax[0].set_title("Approximation")
    ax[1].set_title("Ground truth")
    ax[2].set_title("Relative error")
    ax[0].set_aspect("equal")
    ax[1].set_aspect("equal")
    ax[2].set_aspect("equal")
    fig.tight_layout()
    fig.savefig("helmholtz_nnls_gamma.pdf")

    # Final solution of c(x ,y)
    fig, ax = plt.subplots(1, 3, figsize=(7, 3))
    fig.suptitle(r"Solution $u(x)$ on test data")
    plt.rcParams["image.cmap"] = "jet"  #'brg'
    extent = [0, 1.5, 0, 1.5]
    aspect = 1.3
    sol_img1 = ax[0].imshow(
        u_approx_test.reshape(n_test, n_test),
        zorder=1,
        extent=extent,
        origin="lower",
        aspect=aspect,
    )
    sol_img2 = ax[1].imshow(
        u_true_test.reshape(n_test, n_test),
        zorder=1,
        extent=extent,
        origin="lower",
        aspect=aspect,
    )
    sol_img3 = ax[2].imshow(
        np.abs(u_true_test - u_approx_test).reshape((n_test, n_test))
        / np.sqrt(mean_squared_error(u_true_test, 0 * u_true_test)),
        zorder=1,
        extent=extent,
        origin="lower",
        aspect=aspect,
    )
    fig.colorbar(sol_img1, ax=ax[0], location="bottom")
    fig.colorbar(sol_img2, ax=ax[1], location="bottom")
    fig.colorbar(sol_img3, ax=ax[2], location="bottom")
    ax[1].scatter(*data_m.T, s=1, zorder=2, marker="+")  # , vmin=vlim[0], vmax=vlim[1]

    ax[0].set_title("Approximation")
    ax[1].set_title("Ground truth + data locations")
    ax[2].set_title("Relative error")
    ax[0].set_aspect("equal")
    ax[1].set_aspect("equal")
    ax[2].set_aspect("equal")
    fig.tight_layout()
    fig.savefig("helmholtz_nnls_u.pdf")


def run_single(
    _random_state,
    regularization_initialcondition,
    regularization_ALS,
    noise_level=0,
    plot=False,
):
    data_m, data_col, data_test = setup_data(_random_state=_random_state)
    u_true_m = generate_noisy_measurements(
        data_m=data_m, _random_state=_random_state, noise_level=noise_level
    )

    t0 = time()
    model_u = setup_sampled_network(
        data_m=data_m,
        u_true_m=u_true_m,
        activation_function="tanh",
        parameter_sampler="tanh",
        _random_state=_random_state,
        regularization_scale=regularization_initialcondition,
    )

    model_gamma = setup_gamma_network(
        data_col=data_col,
        activation_function="tanh",
        parameter_sampler="tanh",
        _random_state=_random_state + 123,
        regularization_scale=regularization_initialcondition,
    )

    model_u, model_gamma = alternating_least_squares(
        data_m=data_m,
        u_true_m=u_true_m,
        data_col=data_col,
        model_u=model_u,
        model_gamma=model_gamma,
        regularization_scale=regularization_ALS,
    )

    time_total = time() - t0

    if plot:
        plot_results(
            data_col=data_col,
            data_m=data_m,
            data_test=data_test,
            model_gamma=model_gamma,
            model_u=model_u,
        )
    return model_gamma, model_u, time_total


def evaluate_errors(_random_state, model_gamma, model_u):
    data_m, data_col, data_test = setup_data(_random_state)

    all_errors = dict()
    for name, data in [("m", data_m), ("col", data_col), ("test", data_test)]:
        all_errors[f"l2-{name}-gamma"] = l_2(model_gamma.transform(data), gamma(data))[
            0
        ]
        all_errors[f"linf-{name}-gamma"] = l_inf(
            model_gamma.transform(data), gamma(data)
        )[0]
        all_errors[f"rmse-{name}-gamma"] = rmse(
            model_gamma.transform(data), gamma(data)
        )[0]
        all_errors[f"l2-{name}-u"] = l_2(
            model_u[1].transform(model_u[0].evaluate_model(data)), u_true(data)
        )[0]
        all_errors[f"linf-{name}-u"] = l_inf(
            model_u[1].transform(model_u[0].evaluate_model(data)), u_true(data)
        )[0]
        all_errors[f"rmse-{name}-u"] = rmse(
            model_u[1].transform(model_u[0].evaluate_model(data)), u_true(data)
        )[0]

    return all_errors


if __name__ == "__main__":
    create_figures = (
        True  # for the single figure illustrating the measurement placements
    )
    run_noise_test = (
        False  # for the experiment regarding different noise level on measurements
    )
    run_error_test = False  # for the table in the paper

    if create_figures:
        noise_level = 0.0
        regularization_ALS = 0
        regularization_initialcondition = 0
        model_gamma, model_u, time_total = run_single(
            random_state, noise_level=noise_level, plot=True, regularization_ALS=regularization_ALS, regularization_initialcondition=regularization_initialcondition
        )
    if run_noise_test:
        n_tests = 1
        regularization_initialcondition = 1e-8
        regularization_ALS = 1e-6
        noise_levels = np.linspace(0, 0.1, 3)
        all_errors_mean = []
        all_errors_std = []
        for noise_level in noise_levels:
            current_errors = []
            for k_test in range(n_tests):
                print(f"Test {k_test+1} / {n_tests} at noise level {noise_level}...")
                random_state = 12345 + k_test + int(noise_level * 1000000)
                model_gamma, model_u, time_total = run_single(
                    _random_state=random_state,
                    noise_level=noise_level,
                    plot=False,
                    regularization_initialcondition=regularization_initialcondition,
                    regularization_ALS=regularization_ALS,
                )
                experiment_data = evaluate_errors(
                    _random_state=random_state, model_gamma=model_gamma, model_u=model_u
                )
                experiment_data["time-solve"] = time_total
                experiment_data["noise-level"] = noise_level
                current_errors.append(experiment_data)
            df = pd.DataFrame.from_dict(current_errors)
            all_errors_mean.append(df.mean().to_dict())
            all_errors_std.append(df.std().to_dict())
        df_all_mean = pd.DataFrame.from_dict(all_errors_mean)
        df_all_std = pd.DataFrame.from_dict(all_errors_std)

        print(df_all_std)

        fig, ax = plt.subplots(1, 1, figsize=(4, 3))
        for error_name in ['l2-test-gamma', 'l2-test-u']:
            ax.fill_between(x=np.arange(df_all_mean.shape[0]), y1=df_all_mean[error_name]-df_all_std[error_name], y2=df_all_mean[error_name]+df_all_std[error_name], alpha=.5)

        df_all_mean.plot(ax=ax, x="noise-level", y="l2-test-u", label=r"Solution $u$")
        df_all_mean.plot(
            ax=ax, x="noise-level", y="l2-test-gamma", label=r"Parameter field $\gamma$"
        )

        ax.set_xlabel(r"Noise level $\epsilon$")
        ax.set_ylabel(r"Relative $L^2$ error")

        fig.tight_layout()
        fig.savefig("helmholtz_als_noise.pdf")

    if run_error_test:
        n_tests = 3
        noise_level = 0.0
        regularization_ALS = 0
        regularization_initialcondition = 0

        all_errors = []
        for k_test in range(n_tests):
            print(f"Test {k_test+1} / {n_tests}...")
            random_state = 12345 + k_test
            model_gamma, model_u, time_total = run_single(
                random_state,
                noise_level=noise_level,
                plot=False,
                regularization_ALS=regularization_ALS,
                regularization_initialcondition=regularization_initialcondition,
            )
            experiment_data = evaluate_errors(
                _random_state=random_state, model_gamma=model_gamma, model_u=model_u
            )
            experiment_data["time-solve"] = time_total
            all_errors.append(experiment_data)
        df = pd.DataFrame.from_dict(all_errors)

        print("Solve time mean")
        print(df.mean()[["time-solve"]])
        print("Solve time std")
        print(df.std()[["time-solve"]])

        print("Errors mean")
        print(
            df.mean()[
                [
                    "l2-test-gamma",
                    "linf-test-gamma",
                    "rmse-test-gamma",
                    "l2-test-u",
                    "linf-test-u",
                    "rmse-test-u",
                ]
            ]
        )

        print("Errors std")
        print(
            df.std()[
                [
                    "l2-test-gamma",
                    "linf-test-gamma",
                    "rmse-test-gamma",
                    "l2-test-u",
                    "linf-test-u",
                    "rmse-test-u",
                ]
            ]
        )
