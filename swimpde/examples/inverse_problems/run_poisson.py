import sys

sys.path.append("../../")
sys.path.append("../../src")

from swimpde import Domain
from swimpde import BasicAnsatz
from swimpde import PoissonSolver
import numpy as np
import matplotlib.pyplot as plt
from time import time
from scipy.optimize import minimize_scalar
from sklearn.metrics import mean_squared_error
import pandas as pd


n_dim = 2
n_points = 900
xlim = [0, 1.4]

def l_2_relative(v, v_true):
    v = v.ravel()
    v_true = v_true.ravel()
    return np.sqrt(
        mean_squared_error(v_true, v)
        / mean_squared_error(v_true, np.zeros_like(v_true))
    ).ravel()

def define_domain(_random_state, n_points, n_points_boundary):
    rng = np.random.default_rng(_random_state)

    interior_points = rng.uniform(low=xlim[0], high=xlim[1], size=(n_points, n_dim))

    # construct boundary of the square
    boundary_points_1 = rng.uniform(
        low=xlim[0], high=xlim[1], size=(n_points_boundary // 2, n_dim)
    )
    boundary_points_1[: n_points_boundary // 4, 0] = xlim[0]
    boundary_points_1[n_points_boundary // 4 :, 0] = xlim[1]
    boundary_points_2 = rng.uniform(
        low=xlim[0], high=xlim[1], size=(n_points_boundary // 2, n_dim)
    )
    boundary_points_2[: n_points_boundary // 4, 1] = xlim[0]
    boundary_points_2[n_points_boundary // 4 :, 1] = xlim[1]

    boundary_points = np.row_stack([boundary_points_1, boundary_points_2])

    domain = Domain(interior_points=interior_points, boundary_points=boundary_points)
    return domain


def u_true(xyz):
    return np.sin(xyz[:, 0] ** 2 * np.pi) * np.sin(xyz[:, 1] ** 2 * np.pi)


def forcing(xyz):
    x = xyz[:, 0]
    y = xyz[:, 1]
    return (
        2
        * np.pi
        * (
            np.cos(y**2 * np.pi) * np.sin(np.pi * x**2)
            + (np.cos(np.pi * x**2) - 2 * np.pi * (x**2 + y**2) * np.sin(np.pi * x**2))
            * np.sin(np.pi * y**2)
        )
    ).reshape((-1, 1))


def boundary(xyz):
    return u_true(xyz).reshape((-1, 1))


def setup_data(_random_state, n_measurement_points, noise_level=0):
    rng = np.random.default_rng(_random_state)
    measurement_points = rng.uniform(
        low=xlim[0], high=xlim[1], size=(n_measurement_points, n_dim)
    )
    u_measured = u_true(measurement_points).reshape((-1, 1)) + noise_level * rng.normal(
        loc=0, scale=1, size=(n_measurement_points, 1)
    )

    return measurement_points, u_measured

def setup_test_data(n_points_test = 101):
    x_test_linspace = np.linspace(*xlim, int(np.sqrt(n_points_test)))
    xx_test, yy_test = np.meshgrid(x_test_linspace, x_test_linspace)
    x_test = np.column_stack([xx_test.ravel(), yy_test.ravel()])
    return x_test

def setup_domain_ansatz(_random_state, n_neurons=1024):
    ansatz = BasicAnsatz(
        activation="tanh",
        parameter_sampler='tanh',
        n_neurons=n_neurons,
        random_state=_random_state,
        regularization_scale=1e-10
    )
    return ansatz


def plot_inverse_problem(_random_state=1234, n_measurement_points=50, plot=False):
    # Now we define the inverse problem.
    # This means sampling a few points in 2D where we know the true solution,
    # and also to define the loss function on them, given a value of the parameter (here: the scaling of the second derivative of u w.r.t. y)

    measurement_points, u_measured = setup_data(
        _random_state=_random_state, n_measurement_points=n_measurement_points
    )

    if plot:
        cmap = "jet"
        n_points_test = 100
        x_test = setup_test_data(n_points_test=n_points_test)

        ticklabels = [0, 0.7, 1.4]
        extent = [ticklabels[0], ticklabels[-1]] * 2

        fig, ax = plt.subplots(1, 1, figsize=(3, 3))
        plots = ax.imshow(
            u_true(x_test).ravel().reshape((n_points_test, n_points_test)),
            extent=extent,
            origin="lower",
            cmap=cmap,
        )
        ax.scatter(*measurement_points.T, s=2, c="black")
        ax.set_aspect(1)
        ax.set_xticks(ticklabels)
        ax.set_yticks(ticklabels)
        ax.set_xlabel(r"$x_1$")
        ax.set_ylabel(r"$x_2$")
        # fig.colorbar(plots, ax=ax)
        ax.set_title(f"Ground truth u(x)")

        fig.tight_layout()
        fig.savefig("inverse_poisson_ground_truth.pdf")


def solve_poisson_problem(alpha, ansatz, domain_local, boundary_local):
    poisson_solver = PoissonSolver(
        parameter_scaling=np.array([1, alpha]),
        domain=domain_local,
        f=forcing,
        g=boundary_local,
        ansatz=ansatz,
    )
    poisson_solver.fit()
    return poisson_solver


def nonlinear_loss(
    alpha, ansatz, measurement_points, u_measured, domain_local, boundary_local
):
    poisson_solver = solve_poisson_problem(alpha, ansatz, domain_local, boundary_local)
    return np.linalg.norm(
        poisson_solver.evaluate(measurement_points).ravel() - u_measured.ravel()
    )


def solve_all_alphas(
    _random_state, measurement_points, u_measured, n_alphas=51, n_points_sqrt=30
):
    # plot the loss over alpha, for different number of points in the domain
    alphas = np.linspace(0, 2, n_alphas)
    domain_local = define_domain(
        _random_state=_random_state,
        n_points=n_points_sqrt * n_points_sqrt,
        n_points_boundary=5,
    )
    domain_local.boundary_points = measurement_points
    boundary_local = lambda xy: u_measured
    losses = [
        nonlinear_loss(
            alpha=alpha,
            domain_local=domain_local,
            boundary_local=boundary_local,
            measurement_points=measurement_points,
            u_measured=u_measured,
            ansatz=setup_domain_ansatz(_random_state=_random_state + i),
        )
        for (i, alpha) in enumerate(alphas)
    ]
    return alphas, losses


def solve_alphas_with_optimization(
    _random_state,
    measurement_points,
    u_measured,
    n_points_sqrt=30,
    n_points_boundary=5,
    n_neurons=1024,
):
    domain_local = define_domain(
        _random_state=_random_state,
        n_points=n_points_sqrt * n_points_sqrt,
        n_points_boundary=n_points_boundary,
    )
    domain_local.boundary_points = measurement_points
    boundary_local = lambda xy: u_measured

    def to_optimize(alpha):
        return nonlinear_loss(
            alpha=alpha.ravel()[0],
            domain_local=domain_local,
            boundary_local=boundary_local,
            measurement_points=measurement_points,
            u_measured=u_measured,
            ansatz=setup_domain_ansatz(
                n_neurons=n_neurons,
                _random_state=_random_state + int(1000000 * alpha.ravel()[0]),
            ),
        )

    tol = 1e-15
    sol = minimize_scalar(to_optimize, bounds=(0, 2.0))
    return sol


def plot_result():
    n_measurement_points = 50
    _random_state = 1234
    n_neurons = 1024
    # how many points do we use to solve the PDE in the domain (per side)?
    n_points_sqrt = 30
    measurement_points, u_measured = setup_data(
        _random_state=_random_state, n_measurement_points=n_measurement_points
    )
    n_tests = 5 # number of random seeds to average over

    experiments = []
    for k_test in range(n_tests):
        print(f"Running test {k_test+1} / {n_tests}...")
        # first we solve using minimize_scalar
        t0 = time()
        solver_data = solve_alphas_with_optimization(
            _random_state + k_test + 1123,
            measurement_points,
            u_measured,
            n_points_sqrt=n_points_sqrt,
            n_neurons=n_neurons,
        )
        t_solve = time() - t0

        # then we compute the error at the found solution
        ansatz = setup_domain_ansatz(
            n_neurons=n_neurons,
            _random_state=_random_state + k_test + 2123,
        )
        domain_local = define_domain(
            _random_state=_random_state + k_test + 3123,
            n_points=n_points_sqrt * n_points_sqrt,
            n_points_boundary=1,  # this is reset below anyway, so it does not matter here
        )
        domain_local.boundary_points = measurement_points
        # we set the "boundary" data to just the measurement points
        boundary_local = lambda xy: u_measured

        poisson_solver = solve_poisson_problem(
            alpha=solver_data.x,
            ansatz=ansatz,
            domain_local=domain_local,
            boundary_local=boundary_local,
        )

        n_points_test = 10000
        x_test = setup_test_data(n_points_test=n_points_test)
        loss_rmse = np.sqrt(mean_squared_error(poisson_solver.evaluate(x_test), u_true(x_test)))
        loss_relative = l_2_relative(poisson_solver.evaluate(x_test), u_true(x_test))[0]
        alpha_k = solver_data.x

        experiments.append({
            'error_rmse_PDE': loss_rmse,
            'error_relative_PDE': loss_relative,
            'alpha': alpha_k,
            'time-alpha': t_solve
        })

    df = pd.DataFrame.from_dict(experiments)

    print("MEAN OVER ALL TESTS")
    print(df.mean())

    print("STD OVER ALL TESTS")
    print(df.std())

    # then we plot the full loss curve
    print_loss_curve = False

    if print_loss_curve:
        n_alphas = 51
        alphas, losses = solve_all_alphas(
            _random_state=_random_state,
            measurement_points=measurement_points,
            u_measured=u_measured,
            n_alphas=n_alphas,
            n_points_sqrt=30,
        )

        fig, ax = plt.subplots(1, 1, figsize=(5, 3))

        ax.semilogy(alphas, losses)
        ax.set_ylabel(rf"Loss at {n_measurement_points} measurement points")
        ax.set_xlabel(r"$\alpha$")
        fig.tight_layout()
        fig.savefig("inverse_poisson_alpha_loss.pdf")


plot_result()
