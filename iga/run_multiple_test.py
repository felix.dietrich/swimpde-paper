import argparse
from src import tests
from src import utils


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="iga",
        description="Solve PDEs with the IGA method."
    )

    parser.add_argument(
        "--config",
        type=str,
        help="Path to the .json file specifying the configuration."
    )
    
    args = parser.parse_args()

    multiple_test_dict = utils.parse_json(args.config)
    multiple_test_config = utils.TestConfig.from_dict(config_dict=multiple_test_dict)

    multiple_test = tests.MultipleTest(test_config=multiple_test_config)
    multiple_test.run_tests()
    multiple_test.plot_results()

