Solving partial differential equations with sampled neural networks
========================================

This repository contains the source code for our paper (`arxiv`_ pre-print) on solving PDE with sampled neural networks [1]. An updated code base can be found in the swimpde `repository`_.

Structure
------------
- **data/** contains data for all the experiments.
- **iga/** contains experiments with IGA-FEM.
- **pinns/** contains experiments with PINNS.
- **swimpde/** contains experiments with SWIM-PDE package.

Installation and execution
------------
Please refer to *README.md* files of the subdirectories for how to install the required packages and reproduce the experiments.

Citation
--------

If you use the SWIM-PDE package in your research, please cite the following papers:

.. [1] C\. Datar, T. Kapoor, A. Chandra, Q. Sun, I. Burak, E. Bolager, A. Veselovska, M. Fornasier, F. Dietrich. Solving partial differential equations with sampled neural networks, pre-print, 2024; arxiv:2405.20836.
.. [2] E\. Bolager, I. Burak, C. Datar, Q. Sun, F. Dietrich. Sampling weights of deep neural networks. NeurIPS 2023; arXiv:2306.16830.

.. _repository: https://gitlab.com/felix.dietrich/swimpde
.. _arxiv: http://arxiv.org/abs/2405.20836