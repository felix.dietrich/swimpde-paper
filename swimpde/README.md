SWIM-PDE is a framework for solving partial differential equations using neural networks. The solvers approximate the solution by a linear combination of basis functions, that constitue a shallow neural network. The weights and biases are then chosen to fit the PDE in a discrete set of points in the domain.
The weights of the neural networks are sampled based on the SWIM algorithm ([1]). This speeds up the fitting procedure, as no iterative optimization algorithm is required.

## Installation
---
In order to install this package, clone the repository and run 
```bash
pip install .
```
in the root directory. You may need to install [swimnetworks](https://gitlab.com/felix.dietrich/swimnetworks) first.

[1]: http://arxiv.org/abs/2306.16830 "Sampling weights of deep neural networks"

## Features and concept
Solving a PDE with this package is built on three main components: a domain, an ansatz and a solver. The domain is represented by collections of points of arbitrary shape, which makes the package very flexible in this regard. The solver and ansatz classes are designed so that in principle, any solver can be combined with any ansatz. The ansatz classes represent basis functions (in space) that comprise a (simple) neural network and know how to fit the NN to data, as well as how to evaluate the network as-is or with differential operators applied to it. The solvers use this interface to fit the parameters in these basis functions to solve specific types of PDE. They are also responsible for the outermost set of coefficients used to construct a solution from the set of basis functions, since these may be time-dependent. Note that while all ansatz classes implement the same interface, it may not make sense to use them with any solver as they can have specific requirements.

## Directory structure
- The directory 'data' contains the Stanford bunny dataset. 
- The directory 'examples' contains the examples in the paper.
    - In this work, we have 5 examples - advection equation, burgers' equation, helmholtz equation on a complex geometry, Euler Bernoulli beam equation and inverse problems.  
- The directory 'src' contains all the source files for the code.

## How to run the examples
- Open notebooks in the examples directory and run the cells.  