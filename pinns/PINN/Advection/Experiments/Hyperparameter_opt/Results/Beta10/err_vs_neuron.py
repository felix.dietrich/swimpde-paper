import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter

# Define neurons and error arrays
neurons = [10, 20, 30, 40]

RMSE_mean = [0.0012, 0.00065, 0.00036, 0.00038]
RMSE_std = [0.0002, 0.00025, 0.000043, 0.000093]

rel_err_mean = [0.0017, 0.00092, 0.00052, 0.00054]
rel_err_std = [0.0003, 0.00036, 0.000061, 0.00013]

# Create a figure
plt.figure(figsize=(10, 6))

# Plot RMSE mean with standard deviation bars
plt.errorbar(neurons, RMSE_mean, yerr=RMSE_std, fmt='-o', color='b', ecolor='b', elinewidth=3, capsize=7, label='RMSE',
             linestyle='-', linewidth=5, marker='o', markersize=8, markerfacecolor='blue', markeredgewidth=2)

# Plot relative error mean with standard deviation bars
plt.errorbar(neurons, rel_err_mean, yerr=rel_err_std, fmt='-s', color='r', ecolor='r', elinewidth=3, capsize=7, label='Relative Error',
             linestyle='--', linewidth=5, marker='s', markersize=8, markerfacecolor='red', markeredgewidth=2)

# Adding labels and title with increased font size
plt.xlabel('Width of hidden layer', fontsize=30)
plt.ylabel('Error', fontsize=30)

# Set x-axis ticks to only the values in the neurons array and increase font size
plt.xticks(neurons, fontsize=30)

# Set y-axis ticks to be in scientific notation ('e') and increase font size
plt.gca().yaxis.set_major_formatter(ScalarFormatter(useMathText=True))
plt.tick_params(axis='y', labelsize=30)

# Adding a legend without a box and increase font size
plt.legend(frameon=False, fontsize=30)

# Show the plot
plt.tight_layout()
plt.savefig('error_vs_neurons.png', dpi=300, bbox_inches="tight")
plt.show()

