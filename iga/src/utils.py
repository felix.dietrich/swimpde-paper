import json
from dataclasses import dataclass
from typing import List


@dataclass
class TestConfig:
    """The configuration parameters for the convergence test
    
    Attributes:
    -----------
    pde: str
        Name of the PDE
    beta: List[float]
        Constant parameter in the PDE
    n: List[int]
        Number of nodes in IGA
    k: List[int]
        Order of polynomials in IGA
    """
    pde: str
    list_beta: List[float] | None
    list_n: List[int]
    list_k: List[int]
    output_filename: str
    output_figurename: str
    output_name: str
    plot_type: str

    @classmethod
    def from_dict(cls, config_dict: dict):
        if config_dict["pde"] == "advection_periodic":
            list_beta = config_dict["beta"]
        else:
            list_beta = None
        output_filename = "outputs/" + config_dict["output_name"] + ".json"
        output_figurename = "outputs/" + config_dict["output_name"] + ".png"
        output_name = config_dict["output_name"]
        if "plot_type" in config_dict.keys():
            plot_type = config_dict["plot_type"]
        else:
            plot_type = "error"
        return cls(
            config_dict["pde"],
            list_beta,
            config_dict["n"],
            config_dict["k"],
            output_filename,
            output_figurename,
            output_name,
            plot_type
        )

def parse_json(filename: str) -> dict:
    with open(filename) as f:
        content = json.load(f)
    return content

def save_json(filename: str, content: dict):
    with open(filename, "w") as fout:
            json.dump(content, fout, indent=4)