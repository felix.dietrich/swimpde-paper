import pandas as pd
import numpy as np

# Read CSV file and calculate mean and standard deviation
def process_csv(file_name):
    # Read CSV file
    df = pd.read_csv(file_name)
    
    # Extract entries
    entries = df.iloc[:, 0]  # Assuming single column in CSV
    
    # Calculate mean and standard deviation
    mean = np.mean(entries)
    std_dev = np.std(entries)
    
    # Append mean +- std_dev to DataFrame
    df = df.append({'Mean': mean, 'Std Dev': std_dev}, ignore_index=True)
    
    return df

# Main script logic
if __name__ == '__main__':
    csv_file = 'Rel_err_results.csv'
    
    # Process CSV file
    updated_df = process_csv(csv_file)
    
    # Write updated DataFrame back to CSV
    updated_df.to_csv(csv_file, index=False)
    
    csv_file = 'RMSE_results.csv'
    
    # Process CSV file
    updated_df = process_csv(csv_file)
    
    # Write updated DataFrame back to CSV
    updated_df.to_csv(csv_file, index=False)
    
    csv_file = 'Time_results.csv'
    
    # Process CSV file
    updated_df = process_csv(csv_file)
    
    # Write updated DataFrame back to CSV
    updated_df.to_csv(csv_file, index=False)

