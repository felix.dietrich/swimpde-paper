import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter

# Define beta and error arrays
beta = [0.01, 0.1, 1, 10, 40, 100]

RMSE_mean = [0.00091, 0.00067, 0.00033, 0.00036, 0.48, 0.66]
RMSE_std = [0.000075, 0.00015, 0.000021, 0.000043, 0.02, 0.01]

rel_err_mean = [0.0012, 0.00095, 0.00046, 0.00052, 0.69, 0.93]
rel_err_std = [0.0001, 0.00022, 0.000029, 0.000061, 0.02, 0.01]

# Create a figure
plt.figure(figsize=(10, 6))

# Plot RMSE mean with standard deviation bars
plt.errorbar(beta, RMSE_mean, yerr=RMSE_std, fmt='-o', color='b', ecolor='b', elinewidth=3, capsize=7, label='RMSE',
             linestyle='-', linewidth=5, marker='o', markersize=8, markerfacecolor='blue', markeredgewidth=2)

# Plot relative error mean with standard deviation bars
plt.errorbar(beta, rel_err_mean, yerr=rel_err_std, fmt='-s', color='r', ecolor='r', elinewidth=3, capsize=7, label='Relative Error',
             linestyle='--', linewidth=5, marker='s', markersize=8, markerfacecolor='red', markeredgewidth=2)

# Adding labels and title with increased font size
plt.xlabel('$β$', fontsize=30)
plt.ylabel('Error', fontsize=30)

# Set x-axis ticks to only the values in the beta array and increase font size
plt.xticks(beta, fontsize=30)

# Set y-axis ticks to be in powers of 10 and increase font size
plt.gca().yaxis.set_major_formatter(ScalarFormatter(useMathText=True))
plt.tick_params(axis='y', labelsize=30)

# Adding a legend without a box and increase font size
plt.legend(frameon=False, fontsize=30)

# Convert plot to log-log scale
plt.xscale('log')
plt.yscale('log')

# Show the plot
plt.tight_layout()
plt.savefig('error_vs_beta_loglog.png', dpi=300, bbox_inches="tight")
plt.show()

