import argparse
from src import tests
from src import utils


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="iga",
        description="Solve PDEs with the IGA method."
    )

    parser.add_argument(
        "--config",
        type=str,
        help="Path to the .json file specifying the configuration."
    )
    
    args = parser.parse_args()

    single_test_dict = utils.parse_json(args.config)
    single_test_config = utils.TestConfig.from_dict(config_dict=single_test_dict)

    single_test = tests.SingleTest(test_config=single_test_config)
    single_test.run_test()
    single_test.plot_result()

