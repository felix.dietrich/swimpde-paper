


- (Requires python 3.10)
- Change working directory to *iga/*
- Generate configuration files with ```python src/generate_configs.py```
- Run single test with e.g. ```python run_single_test.py --config config/config_burger_dirichlet_single.json```
- Run multiple tests (convergence test or test for advection equation with different betas) with e.g. ```python run_multiple_test.py --config config/config_advection_periodic_beta_40.json```