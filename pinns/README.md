## Folder Structure

The codes for physics-informed networks are organized into two main folders: `PINN` and `Causal_PINN`. Below is a detailed description of each folder and their contents.

### PINN Folder

The `PINN` folder contains five subfolders, each dedicated to different experiments involving Physics-Informed Neural Networks (PINNs):

1. **Advection**
2. **Burger**
3. **EB**
4. **Helmholtz**
5. **Inverse**

#### 1. Advection

The `Advection` folder contains experiments related to the linear advection equation. It has two subfolders:

- **Hyperparameter_opt**
  - Contains hyperparameter optimization experiments to choose the best layer width and number of collocation (interior) points.
  - **Running the experiments:**
    - To run with a specific number of collocation points: `python3 LA_beta10_collocation.py num_colloc`
      - Example: `python3 LA_beta10_collocation.py 2000` (for 2000 interior points)
    - To run with a specific layer width: `python3 LA_beta10_neuron.py num_neurons`
      - Example: `python3 LA_beta10_neuron.py 30` (for 30 neurons)
- **Varying_beta**
  - Contains code for running the advection equation with different beta values.
  - **Running the experiments:**
    - `python3 LA_varying_beta.py beta_value`
      - Example: `python3 LA_varying_beta.py 10` (for beta value 10)

- **Results and Trained Models**
  - Each subfolder within an equation contains:
    - **Results**: Figures, errors, and runtimes in CSV format.
    - **Trained Models**: Saved models after training.

- **PINN.py**
  - Contains the model PINN class.

#### 2. Burger

The `Burger` folder contains PINN experiments for the Burgers' equation.

- **Running the experiments:**
  - `python3 Burgers.py`

#### 3. EB

The `EB` folder contains PINN experiments for the Euler-Bernoulli equation.

- **Running the experiments:**
  - `python3 EB.py`

#### 4. Helmholtz

The `Helmholtz` folder contains PINN experiments for two Helmholtz complicated geometry problem.

- **Running the experiments:**
  - `python3 exp.py` for running the exponential solution case
  - `python3 sine.py` for running the trigonometric solution case

#### 5. Inverse

The `Inverse` folder contains PINN experiments for the inverse Poisson equation.

- **Running the experiments:**
  - `python3 poisson_inv_pinn.py`

### Causal_PINN Folder

The `Causal_PINN` folder contains codes to reproduce advection and Burgers' codes using a causal PINN approach.

## General Notes

- All codes, when run, will use the trained models to output the reported results.
- To retrain the networks, ensure to store old models elsewhere as the current code replaces the previous trained models, figures, and appends to the error and time CSV files.
- For retraining, uncomment the training part containing the block `"history"` and comment out the load model line.

